package cat.itb.myintents;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private static final int PHONE_CALL_CODE = 420;
    private EditText editTextPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        editTextPhone = findViewById(R.id.phoneNumber);

        findViewById(R.id.buttonCall).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String phoneNumber = editTextPhone.getText().toString();
                if(!phoneNumber.isEmpty())
                {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        if(checkPermission(Manifest.permission.CALL_PHONE))
                        {
                            Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                            if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) startActivity(i);
                        }
                        else if(!shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                        else
                        {
                            Toast.makeText(MainActivity.this, "Please, enable permissiooooon", Toast.LENGTH_SHORT).show();
                            Intent i2 = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i2.addCategory(Intent.CATEGORY_DEFAULT);
                            i2.setData(Uri.parse("package:"+getPackageName()));
                            i2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i2.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i2.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(i2);
                        }
                    }
                    else
                        olderVersions(phoneNumber);
                }
                else
                    Toast.makeText(MainActivity.this, "Insert a phone number, please", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void olderVersions(String phoneNumber)
    {
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
        if(checkPermission(Manifest.permission.CALL_PHONE)) startActivity(intentCall);
        else Toast.makeText(this, "Need permissions", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case PHONE_CALL_CODE:
                if(permissions[0].equals(Manifest.permission.CALL_PHONE))
                {
                    if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    {
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+editTextPhone.getText().toString()));
                        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) startActivity(intentCall);
                    }
                }
                break;
            default: super.onRequestPermissionsResult(requestCode, permissions, grantResults); break;
        }
    }

    private boolean checkPermission(String permission) { return this.checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED; }
}